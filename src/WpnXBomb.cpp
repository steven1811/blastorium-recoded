#include "WpnXBomb.h"

const int B_MAP_LENGTH = 17;	//self
const int B_MAP_HEIGHT = 15;	//explanatory

const int BMBDUR = 75;		//2.5 seconds	- pre-explosion duration. 
const int BMBEXP = 30;		//1 second		- explosion duration.
const int BMBDMG = 6;			//damage per tick (30 ticks per second) total of 180 damage

using namespace rapidxml;

void XbombManager::PutBomb(int id, int bomblevel, int row, int col) {
	sf::Time timeElapsed = cooldownTimer.getElapsedTime();

	if (timeElapsed.asSeconds()>COOLDOWN_XBOMB_S) {
		cooldownTimer.restart();
		int nrow = (row + 16) / 32, ncol = (col - 160 + 16) / 32;
		if (BombMap[nrow][ncol]) return;

		//////printf("put a bomb on %d %d, results in %d %d\n",row,col,nrow,ncol);

		int limit = min(bomblevel, 4) + 1;
		if (BombList[id].size()<limit) {
			Globals->GlobalDataManager->UseWeapon(id);
			BombList[id].push_back(Bomb(id, bomblevel, nrow, ncol));
			BombMap[nrow][ncol] = true;
		}
	}

}

void XbombManager::Explode(int row, int col, int srcdir, int level) {
	//spreads fire starting in [row][col] to all directions instead of srcdir
	//fire distance depends on level
	//srcdir -> 1=up, 2=right, 3=down, 4=left, 0=null(all directions ok)
	//remember fire shapes!swdwd
	//from 0..6 respectively: +,|,-,^,>,V,< 
	int drow[5] = { 0,-1,1,-1,1 }, dcol[5] = { 0,1,1,-1,-1 };	//Zeile und Spalte
															//standard:		int drow[5]={0,-1,0,1,0},dcol[5]={0,0,1,0,-1};
	Globals->GlobalDataManager->UpdateWeapon(1, 1);

	//if (ExplosionList.empty()) DEBUGGER_COUNTER.restart();
	ExplosionList.push_back(Explosion(BMBEXP, BMBDMG, row, col, 0));

	FireMap[row][col] += BMBDMG;
	JustExploded[row][col] = true;

	for (int dir = 1; dir <= 4; ++dir) if (dir != srcdir) {						//determine direction
		for (int len = 1; len <= 2 * (min(4, level)); ++len) {					//determine length
			int newrow = row + (len*drow[dir]), newcol = col + (len*dcol[dir]); //new positions 

			if (newrow<0 || newrow >= B_MAP_HEIGHT || newcol<0 || newcol >= B_MAP_LENGTH || JustExploded[newrow][newcol]) break;
			int spritenum;

			if (BombMap[newrow][newcol]) {								//if there's a bomb, explode that one instead
				for (int id = 0; id<2; ++id) {
					int ite = 0;
					while (ite<BombList[id].size()) {
						std::pair<int, int> now = std::make_pair(BombList[id][ite].row_pos, BombList[id][ite].col_pos);
						if (now.first == newrow && now.second == newcol) {
							BombMap[now.first][now.second] = false;
							Globals->GlobalLevel->SoftMap[now.first][now.second] = 0;
							int nexlvl = BombList[id][ite].level;
							BombList[id].erase(BombList[id].begin() + ite);
							Explode(now.first, now.second, 1 + ((dir + 1) % 4), nexlvl);
						}
						else ++ite;
					}
				}
				break;
			}
			else if (Globals->GlobalLevel->SoftMap[newrow][newcol] == 1) {			//if there's a soft tile, end explosion
				spritenum = dir + 2;	   //^,>,v,< respectively
				FireMap[newrow][newcol] += BMBDMG;
				ExplosionList.push_back(Explosion(BMBEXP, BMBDMG, newrow, newcol, spritenum));
				break;
			}
			else if (Globals->GlobalLevel->SoftMap[newrow][newcol] == 0) {			//if it's empty, continue explosion
				if (len == 2 * (min(4, level))) spritenum = dir + 2;				//if it's the fire's limit, same as soft tile
				else					   spritenum = 1 + (dir + 1) % 2;		//else 1 if vertical, 2 if horizontal
				FireMap[newrow][newcol] += BMBDMG;
				ExplosionList.push_back(Explosion(BMBEXP, BMBDMG, newrow, newcol, spritenum));
			}
			else break;
		}
	}
}