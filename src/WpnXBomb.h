#ifndef _WPNXBOMB_H
#define _WPNXBOMB_H
#include "WpnBomb.h"

#include "SFML/Graphics.hpp"

#include "TextureManager.h"
#include "Tilelist.h"
#include "Level.h"
#include "Player.h"
#include "Globals.h"
#include <vector> 



//forward reference to globals
class GlobalManager;

const unsigned int COOLDOWN_XBOMB_S = 2;

class XbombManager : public virtual BombManager
{
private:
	sf::Clock cooldownTimer;

public:
	void Explode(int row, int col, int level, int srcdir);
	void PutBomb(int id, int bomblevel, int row, int col);
};













#endif 