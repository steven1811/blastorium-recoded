#ifndef _NETWORKMANAGER_H
#define _NETWORKMANAGER_H
#include "Level.h"
#include <SFML/Network.hpp>
#include "Player.h"
#define PORT 54321
#define TIMEOUT_S 5

//Reference from Bomberman64 (N64)
#define Handshake1 "Hello Bomberman!" 
#define Handshake2 "Hi Sirius!"

enum mode { MODE_NONE, MODE_SERVER, MODE_CLIENT };

struct PlayerData
{
	sf::Int32 col,									//player color
		cur_col, cur_row,						//player's current tile coordinate
		nex_col, nex_row;						//player's target tile coordinate (this player will try to move to this position)
	double	now_hp,								//player's current HP, lagging HP (for cosmetic purposes) and max HP
			row_pos, col_pos;					//the player's actual position in the game

};


class Player;

class NetworkManager {
	private:
		std::shared_ptr<GlobalManager>	 Globals;

		//Multiplayer sockets
		sf::UdpSocket target;
		sf::IpAddress targetIP;
		unsigned short targetPort = PORT;

	public:

		sf::IpAddress localIP;

		mode Mode = MODE_NONE;

		NetworkManager();
		void Construct(shared_ptr<GlobalManager> Glo);
		void sendChar(char c);
		char receiveChar();

		bool connectTo(sf::String);
		bool createServer();

		unsigned int sendLevelSeed();
		unsigned int receiveLevelSeed();

		void closeServer();
		bool waitForConnection();

		bool sendPlayerData(const Player &player);
		bool receivePlayerData(PlayerData &data);
};

#endif