#include "NetworkManager.h"

NetworkManager::NetworkManager() {
	target.setBlocking(false);
	localIP= sf::IpAddress::getLocalAddress();
}

void NetworkManager::Construct(shared_ptr<GlobalManager> Glo) {
	Globals = Glo;
}
void NetworkManager::sendChar(char c) {
	sf::Packet packetToSend;
	sf::Uint8 charToSend;

	//Send Netmessage to Connected user
	charToSend = c;
	packetToSend << charToSend;
	std::cout << "Send ASCII: " << c << " To: " << targetIP.toString() << ":" << targetPort << std::endl;
	if (target.send(packetToSend,targetIP, targetPort) != sf::Socket::Done) {
		std::cout << "Error Sending Packet to: " << targetIP.toString() << ":" << targetPort << std::endl;
	}
}
char NetworkManager::receiveChar() {
	sf::Uint8 charReceived;
	sf::Packet packetReceived;

	sf::IpAddress address;
	unsigned short port;

	target.receive(packetReceived, address, port);

	if (packetReceived.getDataSize() == sizeof(char)) {
		packetReceived >> charReceived;
		std::cout << "Received " << char(charReceived) << " from: " << address.toString() << ":" << port << std::endl;
		return charReceived;
	}
	return NULL;
}



unsigned int NetworkManager::sendLevelSeed() {
	//Only works in servermode
	sf::Packet packetToSend;
	unsigned int seed= time(0);

	packetToSend << seed;
	sf::Socket::Status state= target.send(packetToSend,targetIP, targetPort);

	if (state != sf::Socket::Done) {
		std::cout << "Error Sending Seed!" << std::endl;
		return 0;
	}
	else if(state== sf::Socket::Done) {
		std::cout << "SEED SENT: " << seed << " to:" << targetIP.toString() << ":" << targetPort << std::endl;
		return seed;
	}
}


unsigned int NetworkManager::receiveLevelSeed() {
	//Only works in clientmode
	unsigned int seed=0;
	sf::Packet packetReceived;
	sf::IpAddress address;
	unsigned short port;

	target.setBlocking(true);
	target.receive(packetReceived, address, port);

	if (packetReceived.getDataSize() == sizeof(unsigned int)) {
		packetReceived >> seed;
		std::cout << "SEED RECEIVED: " << seed << " from: " << address.toString() << ":" << port << std::endl;
	}
	target.setBlocking(false);
	return seed;
}

//Acts as client function
bool NetworkManager::connectTo(sf::String address) {
	sf::IpAddress ipAddress(address);
	unsigned short portBind = sf::Socket::AnyPort;
	sf::Packet packet;
	sf::String str = Handshake1;
	packet << str;
	bool retVal;

	sf::IpAddress receivedIP;
	unsigned short receivedPort;

	closeServer();

	//Bind UDP Sock
	if (target.bind(portBind) != sf::Socket::Done) {
		std::cout << "Couldn't bind UDP Socket to Port " << portBind << std::endl;
	}
	else {
		sf::Socket::Status state = target.send(packet, ipAddress, PORT);
		if (state == sf::Socket::Done) {
			std::cout << "Sent " << str.toAnsiString() << " to: " << address.toAnsiString() << ":" << PORT << std::endl;
			//Wait for Handshake response
			packet.clear();
			target.setBlocking(true);
			target.receive(packet, receivedIP, receivedPort);
			target.setBlocking(false);

			packet >> str;
			std::cout << "Received " << str.toAnsiString() << " from: " << receivedIP.toString() << ":" << receivedPort << std::endl;
			if (str == Handshake2) {
				Mode = MODE_CLIENT;
				targetIP = receivedIP;
				targetPort = receivedPort;
				retVal = true;
			}
		}
		else {
			std::cout << "Sending Packet to: " << address.toAnsiString() << ":" << PORT << " failed!" << std::endl;
			retVal = false;
		}
	}
	return retVal;
}

//Acts as target function
bool NetworkManager::createServer() {
	bool retVal;
	closeServer();

	if (target.bind(PORT) != sf::Socket::Done) {
		std::cout << "Couldn't bind UDP Socket to Port 54321!" << std::endl;
		Mode = MODE_NONE;
		retVal = false;
	}
	else {
		std::cout << "Bound UDP Socket to Port 54321!" << std::endl;
		Mode = MODE_SERVER;
		retVal = true;
	}
	return retVal;
}

bool NetworkManager::waitForConnection() {
	bool retVal;
	sf::IpAddress receivedIP;
	unsigned short receivedPort;
	sf::Packet packet;
	sf::String str;

	if (Mode == MODE_SERVER) {
		sf::Socket::Status state =target.receive(packet, receivedIP, receivedPort);
		if (state == sf::Socket::NotReady) {
			std::cout << "Waiting for incomming Connection on Port 54321!" << std::endl;
			retVal = false;
		}
		else if (state == sf::Socket::Done) {
			packet >> str;
			std::cout << "Received " << str.toAnsiString() << " from:" << receivedIP.toString() << ":" << receivedPort << std::endl;
			targetIP = receivedIP;
			targetPort = receivedPort;

			//Validate data and then do the Handshake sequence
			if (str == Handshake1) {
				std::cout << "Begin Handshake!" << std::endl;
				packet.clear();
				str = Handshake2;
				packet << str;
				std::cout << "Sending Handshake String: " << str.toAnsiString() << " to:" << receivedIP.toString() << ":" << receivedPort << std::endl;
				target.send(packet, targetIP, targetPort);
				retVal = true;
			}
			else {
				std::cout << "Unknown Handshake String!" << std::endl;
				retVal = false;
			}
		}
	}
	else {
		retVal = false;
	}

	return retVal;

}

void NetworkManager::closeServer() {
	std::cout << "closeServer() requested!" << std::endl;
	targetIP = sf::IpAddress();
	targetPort = 0;
	target.unbind();

	Mode = MODE_NONE;
}

bool NetworkManager::sendPlayerData(const Player &player) {
	sf::Packet packet;

	packet << player.col << player.cur_col << player.cur_row << player.nex_col << player.nex_row << player.now_hp << player.row_pos << player.col_pos;

	bool retVal;

	if (target.send(packet, targetIP, targetPort) != sf::Socket::Done) {
		std::cout << "Error Sending Player Data to: " << targetIP.toString() << ":" << targetPort << std::endl;
		retVal= false;
	}
	else {
		retVal = true;
	}
	return retVal;
}


bool NetworkManager::receivePlayerData(PlayerData &data) {
	sf::Packet packet;

	sf::IpAddress address;
	unsigned short port;

	if (target.receive(packet, address, port) == sf::Socket::Done) {
		std::cout << "Received PlayerData: " << targetIP.toString() << ":" << targetPort << std::endl;
		if (packet >> data.col >> data.cur_col >> data.cur_row >>  data.nex_col >> data.nex_row >> data.now_hp >> data.row_pos >> data.col_pos) {
			return true;
		}
	}
	return false;
}
